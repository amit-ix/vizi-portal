function firebaseStorageSvc(){
	this.uploadFile = function(file, uploadUrl, fileUploadCallback){
		var filePath = uploadUrl + '/' + file.name;
	
		var fbRef = firebase.storage().ref().child(filePath);
		fbRef.put(file).then(function(snapshot){
				fbRef.getDownloadURL().then(function(downloadUrl){
					fileUploadCallback(filePath, downloadUrl);
				});
			
		});
		return true;
	};
	
	this.deleteFile = function(filePath){
		var fbRef = firebase.storage().ref().child(filePath);
		fbRef.delete();
		return true;
	};
	
	this.setImagePath = function(imageStoragePath){
		var fbRef = firebase.storage().ref().child(imageStoragePath);

		return fbRef.getDownloadURL();
	};
};

angular.
  module('subjects').
  service('firebaseStorageSvc', firebaseStorageSvc);
  
angular.
  module('charts').
  service('firebaseStorageSvc', firebaseStorageSvc);
