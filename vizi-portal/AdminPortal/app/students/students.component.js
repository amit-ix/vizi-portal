'use strict';

function StudentModel(key, name, gender, pattern, standard, school, parentName, parentKey, parentType, parentMobile){
	this.key = key;
	this.name = name;
	this.gender = gender;
	this.pattern = pattern;
	this.standard = standard;
	this.school = school;	
	this.parentName = parentName;
	this.parentKey = parentKey;
	this.parentType = parentType;
	this.parentMobile = parentMobile;
};

function ListStudentsController($scope, $http){
	var self = this;
	self.statusText = "Loading...";
	
	self.isIXUser = function(userType){
		return userType == "IX";
	};
	
	self.loadStudentsList = function() {
		self.students = [];
		var allUsers = {};
		var allStandards = {};
		var allPatterns = {};

		firebase.database().ref('metadata/patterns').once('value', function(snapshot){
					snapshot.forEach(function (childSnapshot){
						allPatterns[childSnapshot.key] = childSnapshot.val().name;
					});
				}).then(function(){
						firebase.database().ref('metadata/standards').once('value', function(snapshot){
							snapshot.forEach(function (childSnapshot){
								allStandards[childSnapshot.key] = childSnapshot.val().name;
							});
						})
					}).then(function(){					
						firebase.database().ref('user-data/users').once('value', function(snapshot){
							snapshot.forEach(function (userDetailSnapshot){
								var userType = userDetailSnapshot.val().user_type;
								userType = (userType == undefined || userType == "" || userType == "P") ? "Parent" : userType;
								
								allUsers[userDetailSnapshot.key] = {
												parentName : userDetailSnapshot.val().name,
												parentType : userType,
												parentMobile : userDetailSnapshot.val().mob_number
											};				
								});
								
								firebase.database().ref('user-data/students').orderByChild('school_name').once('value', function(snapshot){
									snapshot.forEach(function (childSnapshot){
										var parentName = allUsers[childSnapshot.val().userID] != undefined || allUsers[childSnapshot.val().userID] != null ? allUsers[childSnapshot.val().userID].parentName : "- Not Entered -";
										var parentType = parentName == "- Not Entered -" ? "-" : allUsers[childSnapshot.val().userID].parentType;
										var parentMobile = parentName == "- Not Entered -" ? "-" : allUsers[childSnapshot.val().userID].parentMobile;
										self.students.push(new StudentModel(
											childSnapshot.key,
											childSnapshot.val().name,
											childSnapshot.val().gender,
											allPatterns[childSnapshot.val().pattern.key],
											allStandards[childSnapshot.val().standard.key],
											childSnapshot.val().school_name,
											parentName,
											childSnapshot.val().userID,
											parentType,
											parentMobile
										));
									});
									
									$scope.$apply(function(){
										self.statusText = "";
									});
								});
						});
			});
	};
	
	// Load list on page load
	self.loadStudentsList();
};

function StudentConsumptionController($scope, $http, $routeParams){
	var self = this;
	self.statusText = "Loading...";
	
	
	self.loadStudentConsumption = function(studentKey){
		var allChapters = {};
		self.totalTimeSpent = 0;
		self.perChapterTime = {};
		
		firebase.database().ref('user-data/students/' + studentKey).once('value', function(snapshot){
			self.studentName = snapshot.val().name;
		});
		
		firebase.database().ref('metadata/chapters').once('value', function(snapshot){
			snapshot.forEach(function(childSnapshot){
				allChapters[childSnapshot.key] = childSnapshot.val().shortCode + ": " + childSnapshot.val().name;
			})
		}).then(function(){
			firebase.database().ref('analytics-data/time-trackers/per-student/' + studentKey).once('value', function(snapshot){
				snapshot.forEach(function(childSnapshot){
					if(childSnapshot.key == "total_time"){
						self.totalTimeSpent = Utils.secondsToHms(childSnapshot.val());
					}
					else{
						self.perChapterTime[allChapters[childSnapshot.key]] = Utils.secondsToHms(childSnapshot.val());
					}
				})
				
				$scope.$apply(function(){
					self.statusText = "";
				});
			});
		});
	};
	
	self.loadStudentConsumption($routeParams.key);
};


angular.
  module('students').
  component('studentsList', {
    templateUrl: 'students/students-list.template.html',
    controller: ListStudentsController
  });

angular.
  module('students').
  component('studentConsumption', {
    templateUrl: 'students/student-consumption.template.html',
    controller: StudentConsumptionController
  });
