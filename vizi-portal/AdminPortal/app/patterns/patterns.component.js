'use strict';

class PatternModel{
	constructor(pat_key, pat_name){
		this.key = pat_key;
		this.name = pat_name;
	}
};

function ListPatternsController($scope, $http){
	var self = this;
	self.statusText = "Loading...";
	self.buttonText = "Create";
	
	self.patterns = [];
	self.newPat = new PatternModel();
	
	self.loadPatternsList = function(){
		self.patterns = [];
		firebase.database().ref('metadata/patterns').once('value', function(snapshot){
			snapshot.forEach(function (childSnapshot){
				var tmpPat = new PatternModel();
				tmpPat.key = childSnapshot.key;
				tmpPat.name = childSnapshot.val().name;
				self.patterns.push(tmpPat);
			});
			$scope.$apply(function(){
				self.statusText = "";
			});
		});
	};
	
	// Load list on page load
	self.loadPatternsList();
	
	self.createUpdatePattern = function (){

		if (self.buttonText == "Create"){
			var patRef = firebase.database().ref('metadata/patterns');
			var newPatRef = patRef.push();
			
			newPatRef.set({
				name : self.newPat.name
			});
			alert("New pattern saved : " + self.newPat.name);
			self.newPat.key = newPatRef.key;
			self.patterns.push(self.newPat);
		}
		else {
			firebase.database().ref('metadata/patterns/' + self.newPat.key)
				.set({
					name : self.newPat.name
				});
			alert("Updated : " + self.newPat.name);
		};
		
		self.loadPatternsList();
		self.resetForm();
	}
	
	self.resetForm = function(){
		self.newPat = new PatternModel();
		self.buttonText = "Create";
	};
	
	self.loadPattern = function(key){
		var patRef = firebase.database().ref('metadata/patterns/' + key)
							.once('value').then(function (snapshot){
								$scope.$apply(function(){
									self.newPat = new PatternModel(snapshot.key,
											snapshot.val().name);
								});
							});
		self.buttonText = "Update";
	};
};

angular.
  module('patterns').
  component('patternsList', {
    templateUrl: 'patterns/patterns-list.template.html',
    controller: ListPatternsController
  });
  
 