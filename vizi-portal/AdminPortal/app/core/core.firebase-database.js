  // Initialize Firebase
  
	// DEV ENVIRONMENT
	//var firebaseAppDomain = "https://vizcharts-dev.firebaseio.com/";
	var devConfig = {
		apiKey: "AIzaSyD2xYQq8cWxHruivo5I0Hiud0kzNz-4LxI",
		authDomain: "vizcharts-dev.firebaseapp.com",
		databaseURL: "https://vizcharts-dev.firebaseio.com",
		projectId: "vizcharts-dev",
		storageBucket: "vizcharts-dev.appspot.com",
		messagingSenderId: "866579687791"
	};

	// PROD ENVIRONMENT
	var firebaseAppDomain = "https://vizi-prod.firebaseio.com/";
	var prodConfig = {
		apiKey: "AIzaSyD8PpzEf9OvO25pu2jLgt6AVAtjG-C5khM",
		authDomain: "vizi-prod.firebaseapp.com",
		databaseURL: "https://vizi-prod.firebaseio.com",
		projectId: "vizi-prod",
		storageBucket: "vizi-prod.appspot.com",
		messagingSenderId: "81187275812"
	  };
	  
	// DEV
	//firebase.initializeApp(devConfig);
	
	// PROD
	firebase.initializeApp(prodConfig);
