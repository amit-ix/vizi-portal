'use strict';

function InAppMsgModel(msg_key, msg_text, msg_time){
	this.key = msg_key;
	this.message = msg_text;
	this.timestamp = msg_time;
};

function InAppMsgsController($scope, $http){
	var self = this;
	self.statusText = "Loading...";
	self.messages = [];
	self.newMsg = new InAppMsgModel();
	
	self.resetForm = function(){
		self.newMsg = new InAppMsgModel();
		self.newMsg.timestamp = new Date().toLocaleString();
	};
	
	self.sendMessage = function(){
		if (self.newMsg.message == ""){
			alert("Enter message text.");
			return;
		}
		
		if (confirm("Do you want to send this In-App message?") == false){
			return;
		}
		var msgRef = firebase.database().ref('master-data/notification').push();
		msgRef.set({
			message : self.newMsg.message,
			timestamp : firebase.database.ServerValue.TIMESTAMP
		});
		
		self.newMsg.key = msgRef.key;
		self.newMsg.timestamp = new Date().toLocaleString();
		self.messages.push(self.newMsg);
		
		self.resetForm();
	};
		
	self.loadMessagesList = function() {
		firebase.database().ref('master-data/notification').once('value', function(snapshot){
			self.messages = [];
			snapshot.forEach(function (childSnapshot){
				self.messages.push(new InAppMsgModel(
					childSnapshot.key, childSnapshot.val().message, new Date(childSnapshot.val().timestamp).toLocaleString()
				));
			});
		  
			$scope.$apply(function(){
				self.statusText = "";
			});
		});
	};
	
	self.deleteMessage = function(key){
		if(confirm("Are you sure to delete this message?") == false)
			return;
		
		self.statusText = "Deleting message for all users";
		
		firebase.database().ref('master-data/notification/' + key).remove();
		firebase.database().ref('user-data/notification-read').once('value', function(snapshot){
			snapshot.forEach(function(childSnapshot){
				if(childSnapshot.child(key).exists()){
					firebase.database().ref('user-data/notification-read/' + childSnapshot.key).remove();
				}
			});
			
			self.loadMessagesList();
		});
	};
	
	// Load list on page load
	self.loadMessagesList();
};

angular.
  module('inappmsgs').
  component('inappmsgsList', {
    templateUrl: 'inappmsgs/inappmsgs.template.html',
    controller: InAppMsgsController
  });
  
 