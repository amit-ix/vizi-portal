'use strict';

function UserModel(key, name, email, mobileNo, city, type){
	this.key = key;
	this.name = name;
	this.email = email;
	this.mobileNo = mobileNo;
	this.userType = type;
	this.city = city;
	this.appVersion = "";	
	this.phoneMake = "";
	this.phoneModel = "";
	this.networkType = "";
	this.osVersion = "";
	this.joinedOn = "";
	this.regSource = "-NA-";
};

function ListUsersController($scope, $http, $routeParams){
	var self = this;
	self.statusText = "Loading...";
	
	self.isIXUser = function(userType){
		return userType == "IX";
	};
	
	self.toggleUserTypeFlag = function(key, name){
		var userRef = firebase.database().ref('user-data/users/' + key + '/user_type');
		userRef.once('value', function(snapshot){
			var toggleUserType = snapshot.val() == "IX" ? "P" : "IX";
			var typeText = toggleUserType == "IX" ? "IX Internal User" : "Parent User";
			userRef.set(toggleUserType);
			
			alert(name + ' is marked as ' + typeText);
		});
		
	};
	
	self.loadUsersList = function() {
		self.users = [];
		var allUsers = [];
		
		self.userTypeLabel = "All Users";
		var userTypeFilter = "ALL";
		if ($routeParams.userType == "P"){
			self.userTypeLabel = "Parents";
		}
		else if ($routeParams.userType == "IX"){
			self.userTypeLabel = "IncubXperts Users";
		}
		
		firebase.database().ref('user-data/users').orderByChild('registeredOn').once('value', function(snapshot){
			snapshot.forEach(function (userDetailSnapshot){
				var userType = userDetailSnapshot.val().user_type;
				userType = (userType == undefined || userType == "" || userType == "P") ? "Parent" : userType;
				
				if ($routeParams.userType == "ALL" || $routeParams.userType == userType) {
					var u = new UserModel(
										userDetailSnapshot.key,
										userDetailSnapshot.val().name,
										userDetailSnapshot.val().email,
										userDetailSnapshot.val().mob_number,
										userDetailSnapshot.val().city,
										userType
									);
						if (userDetailSnapshot.val().user_reg_source != null && userDetailSnapshot.val().user_reg_source != "Select")
							u.regSource = userDetailSnapshot.val().user_reg_source;
					
						if (userDetailSnapshot.val().registeredOn != null)
							u.joinedOn = new Date(userDetailSnapshot.val().registeredOn).toLocaleString();
						
						allUsers.push(u);
					};
				});
				firebase.database().ref('audit-data/user-device-info').once('value', function(snapshot){
					snapshot.forEach(function (userDeviceSnapshot){
						
						for (var i=0; i < allUsers.length;i++){
							if (allUsers[i].key == userDeviceSnapshot.key){
								
								allUsers[i].appVersion = userDeviceSnapshot.val().app_version_name,
								allUsers[i].phoneMake = userDeviceSnapshot.val().phone_make,
								allUsers[i].phoneModel = userDeviceSnapshot.val().model,
								allUsers[i].networkType = userDeviceSnapshot.val().network_type,
								allUsers[i].osVersion = userDeviceSnapshot.val().os
							}
						}
					});
					$scope.$apply(function(){
						self.statusText = "";
						self.users = allUsers;
					});
				});
		});
	};
	
	// Load list on page load
	self.loadUsersList();
};

function UsersSummaryController($scope, $http){
	var self = this;
	self.usersStatusText = "Loading...";
	self.usageStatusText = "Loading...";
	self.totalRegistrations = 0;
	self.totalParents = 0;
	self.ixUsers = 0;
	self.totalStudents = 0;
	self.studCountPerStd = {};
	self.appVersions = {};
	
	self.secondsToHms = function (d) {
		d = Number(d);
		var h = Math.floor(d / 3600);
		var m = Math.floor(d % 3600 / 60);
		var s = Math.floor(d % 3600 % 60);

		var hDisplay = h > 0 ? h + (h == 1 ? " hr, " : " hr, ") : "";
		var mDisplay = m > 0 ? m + (m == 1 ? " min, " : " min, ") : "";
		var sDisplay = s > 0 ? s + (s == 1 ? " sec" : " sec") : "";
		return hDisplay + mDisplay + sDisplay;
	}
	
	self.loadUserSummary = function(){
		self.usersStatusText = "Loading...";
		var allStandards = [];
		
		firebase.database().ref('metadata/standards').once('value', function(snapshot){
				snapshot.forEach(function (childSnapshot){
					allStandards[childSnapshot.key] = childSnapshot.val().name;
				});
			}).then(function(){
				firebase.database().ref('analytics-data/students-per-std').once('value', function(snapshot){
					snapshot.forEach(function (childSnapshot){
						var stdName = allStandards[childSnapshot.key];
						self.studCountPerStd[stdName] = childSnapshot.val();
					});
				});
			}).then(function(){
				firebase.database().ref('analytics-data/app-version-counts').once('value', function(snapshot){
						snapshot.forEach(function (childSnapshot){
							var ver = childSnapshot.key.replace(/-/g, ".");
							self.appVersions[ver] = childSnapshot.val();
						});
					})
					
				firebase.database().ref('analytics-data/user-counts').once('value', function(snapshot){
					$scope.$apply(function(){
						if (snapshot.exists()){
							self.totalRegistrations = snapshot.val().total_users;
							self.ixUsers = snapshot.val().ix_users;
							self.totalParents = snapshot.val().parents;
							self.totalStudents = snapshot.val().total_students;
						}
						self.usersStatusText = "";
					});
				});
			});
	}

	self.loadUsageCounters = function(){
		var allChapterCodes = [];
		var allSubjectNames = [];
		self.usageStatusText = "Loading...";
		self.legacyEvents = "";
		
		
		// Dummy code
		/*
		firebase.database().ref('audit-data/chart-view-event').once('value', function(snapshot){
			self.dummy = [];
			snapshot.forEach(function (childSnapshot){
				
				if (childSnapshot.val().student_id == "-KtGgQ8VUpo3shZDRmua"){
					var dt = new Date(childSnapshot.val().ist_time_stamp);
					self.dummy.push(childSnapshot.key + '--' + dt.toLocaleString() + ' -- ' + childSnapshot.val().chapter_image + ' -- ' + childSnapshot.val().view_duration);
				}
			});
			
		});
		*/
		
		firebase.database().ref('metadata/chapters').once('value', function(snapshot){
					snapshot.forEach(function (childSnapshot){
						allChapterCodes[childSnapshot.key] = childSnapshot.val().shortCode;
					});
				}).then(function(){
						firebase.database().ref('metadata/subjects').once('value', function(snapshot){
							snapshot.forEach(function (childSnapshot){
								allSubjectNames[childSnapshot.key] = childSnapshot.val().name;
							});
						})
					}).then(function(){	
						self.hashPerStandard = {};
						self.hashPerSubject = {};
						self.hashPerChapter = {};
						self.hashPerDay = {};
						self.allDaysTotal = 0;
						
						firebase.database().ref('analytics-data/time-trackers').once('value', function(snapshot){
							if (snapshot.exists())
								self.totalTimeSpent = self.secondsToHms(snapshot.val().total_time_spent);
						});
						
						firebase.database().ref('analytics-data/time-trackers/per-day').orderByValue().once('value', function(snapshot){
							snapshot.forEach(function (childSnapshot){
								self.hashPerDay[childSnapshot.key] = {
									activeUsers : childSnapshot.val().total_active_users,
									timespent : childSnapshot.val().total_time
								};
								self.allDaysTotal = self.allDaysTotal + childSnapshot.val().total_time;
							});
						});
						
						firebase.database().ref('analytics-data/time-trackers/per-standard').orderByValue().once('value', function(snapshot){
							snapshot.forEach(function (childSnapshot){
								self.hashPerStandard[childSnapshot.key] = childSnapshot.val();
							});
						});
						
						firebase.database().ref('analytics-data/time-trackers/per-subject').orderByValue().once('value', function(snapshot){
							snapshot.forEach(function (childSnapshot){
								var subKey = childSnapshot.key.split("~")[1];
								var subName = allSubjectNames[subKey];
								self.hashPerSubject[subName] = childSnapshot.val();
							});
						});
						
						firebase.database().ref('analytics-data/time-trackers/per-chapter').orderByValue().once('value', function(snapshot){
							snapshot.forEach(function (childSnapshot){
								var chapCode = allChapterCodes[childSnapshot.key];
								self.hashPerChapter[chapCode] = childSnapshot.val();
							});
						}).then(function(){
								$scope.$apply(function(){
									self.usageStatusText = "";
								});				
						});
					});
		
	};

	self.resetUsageCounters = function(){
		if (confirm("Do you want to reset Time Tracking counters?") == false)
			return;
		
		self.usageStatusText = "Re-calculating Time Tracking counters";
		self.totalTimeSpent = "";
		
		var timeTotal = 0;
		var perStdTotal = {};
		var perSubTotal = {};
		var perChapTotal = {};
		var perDayTotal = {};
		var perStudTotal = {};
		
		//firebase.database().ref('analytics-data/time-trackers').remove().then(function(){
			firebase.database().ref('audit-data/chart-view-event').once('value', function(snapshot){
				snapshot.forEach(function (childSnapshot){
					var time = parseInt(childSnapshot.val().view_duration);
					if(!isNaN(time)){
						timeTotal = timeTotal + time;
						var imgLocation = childSnapshot.val().chapter_image;
						var parts = imgLocation.split("/");
						
						// Add to pattern_standard total time
						var key = parts[0];
						var stdTotal = perStdTotal[key];
						if (stdTotal == undefined){
							stdTotal = time;
						}
						else{
							stdTotal = stdTotal + time;
						}
						perStdTotal[key] = stdTotal;
						
						// Add to Subject total time
						key = parts[0] + '~' + parts[1];
						var subTotal = perSubTotal[key];
						if (subTotal == undefined){
							subTotal = time;
						}
						else{
							subTotal = subTotal + time;
						}
						perSubTotal[key] = subTotal;
						
						// Add to chapter total
						key = childSnapshot.val().chapter_name;
						var chTotal = perChapTotal[key];
						if (chTotal == undefined){
							chTotal = time;
						}
						else{
							chTotal = chTotal + time;
						}
						perChapTotal[key] = chTotal;
						
						// Add to per-student counters
						key = childSnapshot.val().student_id;
					
						var studChaps = perStudTotal[key];
						if (studChaps == undefined){
							studChaps = {};
							studChaps[childSnapshot.val().chapter_name] = time;
						}
						else{
							var chapTotal = studChaps[childSnapshot.val().chapter_name];
							if(chapTotal == undefined){
								studChaps[childSnapshot.val().chapter_name] = time;
							}
							else {
								studChaps[childSnapshot.val().chapter_name] = chapTotal + time;
							}
						}
						perStudTotal[key] = studChaps;
						
						
						// Add to per-day counters
						var dt = childSnapshot.val().timestamp != undefined ? new Date(childSnapshot.val().timestamp) : new Date(childSnapshot.val().ist_time_stamp);
						
						if (isNaN(dt.getMonth())){
							var tmp = childSnapshot.val().timestamp.replace("IST", "GMT+05:30");
							dt = new Date(tmp);
						}
						if (!isNaN(dt.getMonth())){							
						
							key = (dt.getMonth() + 1) < 10 ? "0" + (dt.getMonth() + 1) : (dt.getMonth() + 1);
							key = key + '-' + (dt.getDate() < 10 ? "0" + dt.getDate() : dt.getDate());
							key = key + '-' + dt.getFullYear();
								
							var dayStuds = perDayTotal[key];
							if (dayStuds == undefined){
								dayStuds = {};
								var studChap = {};
								studChap[childSnapshot.val().chapter_name] = time;
								dayStuds[childSnapshot.val().student_id] = studChap;
							}
							else{
								var studChap = dayStuds[childSnapshot.val().student_id];
								if(studChap == undefined){
									studChap = {};
									studChap[childSnapshot.val().chapter_name] = time;
								}
								else {
									var chapTotal = studChap[childSnapshot.val().chapter_name];
									if(chapTotal == undefined){
										studChap[childSnapshot.val().chapter_name] = time;
									}
									else {
										studChap[childSnapshot.val().chapter_name] = chapTotal + time;
									}
								}
								dayStuds[childSnapshot.val().student_id] = studChap;
							}
							
							perDayTotal[key] = dayStuds;
						}
						else{
							firebase.database().ref('analytics-data/time-trackers/per-day-unprocessed/' + childSnapshot.key).set(childSnapshot.val());
						}
					}
				})
			}).then(function(){
				
				firebase.database().ref('analytics-data/time-trackers/per-standard').remove().then(function(){
					for(var std in perStdTotal){
						firebase.database().ref('analytics-data/time-trackers/per-standard/' + std).set(perStdTotal[std]);
					}
				});
			
				firebase.database().ref('analytics-data/time-trackers/per-subject').remove().then(function(){
					for(var sub in perSubTotal){
						firebase.database().ref('analytics-data/time-trackers/per-subject/' + sub).set(perSubTotal[sub]);
					}
				});
				
				firebase.database().ref('analytics-data/time-trackers/per-chapter').remove().then(function(){
					for(var chap in perChapTotal){
						firebase.database().ref('analytics-data/time-trackers/per-chapter/' + chap).set(perChapTotal[chap]);
					}
				});
				
				firebase.database().ref('analytics-data/time-trackers/per-student').remove().then(function(){
					for(var studChap in perStudTotal){
						var studTotal = 0;
						
						var allChaps = perStudTotal[studChap];
						for(var chap in allChaps){
							firebase.database().ref('analytics-data/time-trackers/per-student/' + studChap + '/' + chap).set(allChaps[chap]);
							studTotal = studTotal + allChaps[chap];
						}
						
						firebase.database().ref('analytics-data/time-trackers/per-student/' + studChap + '/total_time').set(studTotal);
					}
				});
				
				firebase.database().ref('analytics-data/time-trackers/per-day').remove().then(function(){
					for(var dayStud in perDayTotal){
						var dayTotal = 0;
						
						var allStuds = perDayTotal[dayStud];
						for(var studChap in allStuds){
							var studTotal = 0;
							
							var allChaps = allStuds[studChap];
							for(var chap in allChaps){
								firebase.database().ref('analytics-data/time-trackers/per-day/' + dayStud + '/' + studChap + '/' + chap).set(allChaps[chap]);
								studTotal = studTotal + allChaps[chap];
								dayTotal = dayTotal + allChaps[chap];
							}
							
							firebase.database().ref('analytics-data/time-trackers/per-day/' + dayStud + '/' + studChap + '/total_time').set(studTotal);
						}
						
						firebase.database().ref('analytics-data/time-trackers/per-day/' + dayStud + '/total_time').set(dayTotal);
						firebase.database().ref('analytics-data/time-trackers/per-day/' + dayStud + '/total_active_users').set(Object.keys(allStuds).length);
					}
				});
				
				firebase.database().ref('analytics-data/time-trackers/total_time_spent').set(timeTotal).then(function(){
					self.loadUsageCounters();
				});
			});
		//});
	};
	
	self.resetUserSummaryCounters = function(){
		if (confirm("Do you want to reset User counters?") == false)
			return;
		
		self.usersStatusText = "Re-calculating user counters";
		var totalUsers = 0;
		var ixUsers = 0;
		var parentUsers = 0;
		var students = 0;
		var studPerStd = {};
		var appVerCounts = [];
		var userList = [];
		
		firebase.database().ref('user-data/users').once('value', function(snapshot){			
			snapshot.forEach(function (userDetailSnapshot){
				totalUsers += 1;
				userList.push(userDetailSnapshot.key);
				
				if (userDetailSnapshot.val().user_type == "IX"){
					ixUsers += 1;
				}
				else {
					parentUsers += 1;
				}
			})
		}).then(function(){
			firebase.database().ref('audit-data/user-device-info').once('value', function(snapshot){
				appVerCounts = [];
				snapshot.forEach(function (childSnapshot){
					if (userList.indexOf(childSnapshot.key) != -1){
						var key = childSnapshot.val().app_version_name.replace(/[.]/g, "-");
						var app = appVerCounts[key];
						if(app != undefined){
							appVerCounts[key] = app + 1;
						}
						else {
							appVerCounts[key] = 1;
						}		
					}			
				})
				firebase.database().ref('analytics-data/app-version-counts').remove();
				for(var app in appVerCounts){
					firebase.database().ref('analytics-data/app-version-counts/' + app).set(appVerCounts[app]);
				}
			});
		}).then(function(){
			firebase.database().ref('user-data/students').once('value', function(snapshot){
				students = snapshot.numChildren();
				
				snapshot.forEach(function (childSnapshot){
					var stud = studPerStd[childSnapshot.val().standard.key];
					if(stud != undefined){
						studPerStd[childSnapshot.val().standard.key] = stud + 1;
					}
					else {
						studPerStd[childSnapshot.val().standard.key] = 1;
					}						
				})
			}).then(function(){	
				firebase.database().ref('analytics-data/user-counts').set({
					total_users : totalUsers,
					ix_users : ixUsers,
					parents : parentUsers,
					total_students : students
				}).then(function(){
					for(var std in studPerStd){
						firebase.database().ref('analytics-data/students-per-std/' + std).set(studPerStd[std]);
					}
					self.loadUserSummary();
				});
			});
		});
	};
	
	self.importLegacyUsageEvent = function(){
		if(confirm("Do you want to import legacy Chart read events?") == false)
			return;
		self.legacyEvents = "";
		
		firebase.database().ref('audit-data/user-event').once('value', function(snapshot){
			var counter = 0;
			snapshot.forEach(function (childSnapshot){
				if (childSnapshot.val().event_name == "Chapter Image View"){
					firebase.database().ref('audit-data/chart-view-event/' + childSnapshot.key).set(childSnapshot.val());
					firebase.database().ref('audit-data/chart-view-event-backup/' + childSnapshot.key).set(childSnapshot.val());
					firebase.database().ref('audit-data/user-event/' + childSnapshot.key).remove();
					counter++;
				}
			});
			
			$scope.$apply(function(){
				self.legacyEvents = counter;
			});
		})
	}
	
	self.loadUserSummary();
	self.loadUsageCounters();
};

angular.
  module('users').
  component('usersList', {
    templateUrl: 'users/users-list.template.html',
    controller: ListUsersController
  });

angular.
  module('users').
  component('usersSummary', {
    templateUrl: 'users/users-summary.template.html',
    controller: UsersSummaryController
  });
  
 