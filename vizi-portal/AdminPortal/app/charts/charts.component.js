'use strict';

function ChartModel(ch_key, ch_imgPath, ch_seqNo, ch_shortCode, ch_thumb, ch_name){
	this.key = ch_key;
	this.imagePath = ch_imgPath;
	this.seqNo = ch_seqNo;
	this.shortCode = ch_shortCode;
	this.thumb = ch_thumb;
	this.name = ch_name;
};

function ListChartsController($scope, $http, firebaseStorageSvc){
	var self = this;
	self.statusText = "";
	self.fileStatus = "";
	self.buttonText = "Create";
	self.chartPreviewUrl = "";
	self.newChart  = new ChartModel();

	firebase.database().ref('metadata/patterns').once('value', function(snapshot){
		self.patterns = [];
		snapshot.forEach(function (childSnapshot){
			self.patterns.push({
				key : childSnapshot.key,
				name : childSnapshot.val().name
			});
		});
	});
	
	firebase.database().ref('metadata/standards').orderByChild('seqNo').once('value', function(snapshot){
		self.standards = [];
		snapshot.forEach(function (childSnapshot){
			self.standards.push({
				key : childSnapshot.key,
				name : childSnapshot.val().name,
				abbr : childSnapshot.val().abbr
			});
		});
	});
	
	firebase.database().ref('metadata/subjects').once('value', function(snapshot){
		self.subjects = [];
		snapshot.forEach(function (childSnapshot){
			self.subjects.push({
				key : childSnapshot.key,
				name : childSnapshot.val().name
			});
		});
	});
	
	self.loadChaptersList = function(){
		if (self.selPattern == null || self.selStandard == null || self.selSubject == null){
			alert("Please select Pattern, Standard and Subject.");
			return;
		}
		
		self.chapters = [];
		self.charts = [];
		var navPath = self.selPattern.key + '_' + self.selStandard.key + '_' + self.selSubject.key;
		firebase.database().ref('metadata/chapters').orderByChild('nav_path').equalTo(navPath).once('value', function(snapshot){
			snapshot.forEach(function (childSnapshot){
				self.chapters.push({ 
					key : childSnapshot.key,
					name : childSnapshot.val().name,
					no : childSnapshot.val().no
				});
			});
		});
		  
		self.statusText = "";
	};
	
	self.loadChartsList = function(){
		self.resetForm();
		if(self.selChapter == null || self.selChapter.key == undefined){
			return;
		}
		
		self.statusText = "Loading...";
		firebase.database().ref('metadata/chapter-charts/' + self.selChapter.key).orderByChild('seqNo').once('value', function(snapshot){
			self.charts = [];
			snapshot.forEach(function (childSnapshot){
				var chartName = childSnapshot.val().name == undefined || childSnapshot.val().thumb == null ? childSnapshot.val().shortCode : childSnapshot.val().name;
				
				self.charts.push(new ChartModel(childSnapshot.key,
													childSnapshot.val().imagePath,
													childSnapshot.val().seqNo,
													childSnapshot.val().shortCode,
													childSnapshot.val().thumb,
													chartName));
			});
		  
			$scope.$apply(function(){
				self.statusText = "";
			});
		});
	};
	
	self.loadChart = function(key){
		var chartRef = firebase.database().ref('metadata/chapter-charts/' + self.selChapter.key + '/' + key);
		
		chartRef.once('value').then(function (snapshot){
			var chartName = snapshot.val().name == undefined || snapshot.val().thumb == null ? snapshot.val().shortCode : snapshot.val().name;
			
			$scope.$apply(function(){
				self.newChart = new ChartModel(snapshot.key,
					snapshot.val().imagePath,
					snapshot.val().seqNo,
					snapshot.val().shortCode,
					snapshot.val().thumb,
					chartName);
					
				self.chartPreviewUrl = "";
			});	
			
			firebaseStorageSvc.setImagePath(snapshot.val().imagePath).then(function(url){
				$scope.$apply(function(){
					self.chartPreviewUrl = url;
				});	
			});
		});
		
		self.buttonText = "Update";
	};
	
	self.uploadChartImage = function(){
		if (self.selChapter == null){
			alert("Please select Chapter.");
			return;
		}
		self.uploadingNewFile = true;
		
		if ($scope.imgFile == null || $scope.imgFile == undefined){
			if (self.buttonText == "Create"){
				alert("Please select Image file.");
			}
			else {
				self.uploadingNewFile = false;
				self.fileStatus = "No new file selected...";
				self.createUpdateChart(self.newChart.imagePath, self.newChart);
			}
			
			return;
		}
		self.fileStatus = "Uploading file...";
		// Upload file to firebase
		var filePath = self.selPattern.name.substr(0, 2) + '_' + self.selStandard.abbr + '/';
		filePath += self.selSubject.key + '/' + self.selChapter.key;
		
		var uploadStatus = firebaseStorageSvc.uploadFile($scope.imgFile, filePath, self.createUpdateChart);
	};
	
	self.createUpdateChart = function(imgStoragePath, downloadUrl){
		if (self.uploadingNewFile == true){
			self.fileStatus = "File upload successful!";
		}
		
		var code = self.selPattern.name.substr(0, 2) + '_' + self.selStandard.abbr + '_' + self.selSubject.name + '_' + self.selChapter.no + '_' + self.newChart.seqNo;
		
		var thumbPath = self.uploadingNewFile == true ? imgStoragePath.replace("/" + $scope.imgFile.name, "/thumb_" + $scope.imgFile.name) : self.newChart.thumb;
		
		if (self.buttonText == "Create"){
			var chartRef = firebase.database().ref('metadata/chapter-charts/' + self.selChapter.key);
			var newChartRef = chartRef.push();
			
			newChartRef.set({
				imagePath : imgStoragePath,
				seqNo : parseInt(self.newChart.seqNo),
				shortCode : code,
				thumb : thumbPath,
				name : self.newChart.name
			});
			
			self.newChart.key = newChartRef.key;
			self.newChart.shortCode = code;
			alert("New Chart saved : " + self.newChart.shortCode);

			self.charts.push(self.newChart);
		}
		else {
			var oldFilePath = self.newChart.imagePath;
			firebase.database().ref('metadata/chapter-charts/' + self.selChapter.key + '/' + self.newChart.key)
				.set({
					imagePath : imgStoragePath,
					seqNo : parseInt(self.newChart.seqNo),
					shortCode : code,
					thumb : thumbPath,
					name : self.newChart.name
				});
			
			if (self.uploadingNewFile == true){
				firebaseStorageSvc.deleteFile(oldFilePath);
			}
			alert("Updated : " + self.newChart.shortCode);
		}
		
		if(self.newChart.seqNo == 1){
			// Set thumbnail path for Chapter
		
			var chapThumbRef = firebase.database().ref('metadata/chapters/' + self.selChapter.key).child('thumb');
			chapThumbRef.set(thumbPath);
		}
		self.loadChartsList();
		self.resetForm();
	};
	
	self.deleteChart = function(){
		if(self.newChart == null || self.newChart.key == undefined){
			alert("Select chart to delete.");
			return;
		}
		
		if (confirm("Do you want to delete Chart : " + self.newChart.shortCode + "?") == false)
			return;
		
		var chartRef = firebase.database().ref('metadata/chapter-charts/' + self.selChapter.key + '/' + self.newChart.key);
		if (chartRef == null || chartRef == undefined){
			alert("Unable to delete the chart");
			return;
		}
		
		chartRef.remove();
		firebaseStorageSvc.deleteFile(self.newChart.imagePath);	
		firebaseStorageSvc.deleteFile(self.newChart.thumb);	
		
		self.loadChartsList();
		self.resetForm();
	};
	
	self.resetForm = function(){
		self.newChart = new ChartModel();
		self.buttonText = "Create";
		self.fileStatus = "";
		self.imgFile = null;
		self.chartPreviewUrl = "";
	};
};

angular.
  module('charts').
  component('chartsList', {
    templateUrl: 'charts/charts-list.template.html',
    controller: ListChartsController
  });

 