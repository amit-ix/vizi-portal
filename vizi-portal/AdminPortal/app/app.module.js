'use strict';

// Define the `viziAdminApp` module
var viziApp = angular.module('viziAdminApp', [
	'ngRoute',
	'patterns',
	'standards',
	'subjects',
	'chapters',
	'charts',
	'users',
	'students',
	'inappmsgs',
	'consumption'
]);

viziApp.directive('viziFileModel', function ($parse) {
	return {
		restrict: 'A', //the directive can be used as an attribute only

		/*
		 link is a function that defines functionality of directive
		 scope: scope associated with the element
		 element: element on which this directive used
		 attrs: key value pair of element attributes
		 */
		link: function (scope, element, attrs) {
			var model = $parse(attrs.viziFileModel),
				modelSetter = model.assign; //define a setter for viziFileModel

			//Bind change event on the element
			element.bind('change', function () {
				//Call apply on scope, it checks for value changes and reflect them on UI
				scope.$apply(function () {
					//set the model value
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	};
});

angular.module('patterns', []);
angular.module('standards', []);
angular.module('subjects', []);
angular.module('chapters', []);
angular.module('charts', []);
angular.module('users', []);
angular.module('students', []);
angular.module('inappmsgs', []);
angular.module('consumption', []);