'use strict';

function ChapterModel(ch_key, ch_name, ch_no, ch_shortCode, ch_thumb, ch_seqNo){
	this.key = ch_key;
	this.name = ch_name;
	this.no = ch_no;
	this.shortCode = ch_shortCode;
	this.thumb = ch_thumb;
	this.seqNo = ch_seqNo;
};

function ListChaptersController($scope, $http, firebaseStorageSvc){
	var self = this;
	self.statusText = "Select navigation...";
	self.buttonText = "Create";
	self.newChap  = new ChapterModel();

	firebase.database().ref('metadata/patterns').once('value', function(snapshot){
		self.patterns = [];
		snapshot.forEach(function (childSnapshot){
			self.patterns.push({
				key : childSnapshot.key,
				name : childSnapshot.val().name
			});
		});
	});
	
	firebase.database().ref('metadata/standards').orderByChild('seqNo').once('value', function(snapshot){
		self.standards = [];
		snapshot.forEach(function (childSnapshot){
			self.standards.push({
				key : childSnapshot.key,
				name : childSnapshot.val().name,
				abbr : childSnapshot.val().abbr
			});
		});
	});
	
	firebase.database().ref('metadata/subjects').once('value', function(snapshot){
		self.subjects = [];
		snapshot.forEach(function (childSnapshot){
			self.subjects.push({
				key : childSnapshot.key,
				name : childSnapshot.val().name
			});
		});
	});
	
	self.loadChaptersList = function(){
		self.statusText = "Loading...";

		if (self.selPattern == null || self.selStandard == null || self.selSubject == null){
			alert("Please select Pattern, Standard and Subject.");
			return;
		}
		
		var navPath = self.selPattern.key + '_' + self.selStandard.key + '_' + self.selSubject.key;
		firebase.database().ref('metadata/chapters').orderByChild('nav_path').equalTo(navPath).once('value', function(snapshot){
			self.chapters = [];
			snapshot.forEach(function (childSnapshot){
				self.chapters.push({ 
					key : childSnapshot.key,
					name : childSnapshot.val().name,
					seqNo : childSnapshot.val().seqNo
				});
			});
			
			self.chapters.sort(function(c1, c2){
				return c1.seqNo - c2.seqNo;
			});
		  
			$scope.$apply(function(){
				self.statusText = "";
			});
		});
	};
	
	self.loadChapter = function(key){
		var chapRef = firebase.database().ref('metadata/chapters/' + key)
							.once('value').then(function (snapshot){
								$scope.$apply(function(){
									self.newChap = new ChapterModel(snapshot.key,
											snapshot.val().name,
											snapshot.val().no,
											snapshot.val().shortCode,
											snapshot.val().thumb,
											snapshot.val().seqNo);
									self.thumbPreviewUrl = "";
								});
								
								firebaseStorageSvc.setImagePath(snapshot.val().thumb).then(function(url){
									$scope.$apply(function(){
										self.thumbPreviewUrl = url;
									});	
								});
							});
		self.buttonText = "Update";
	};
	
	self.createUpdateChapter = function(){
		if (self.buttonText == "Create"){
			if (self.selPattern == null || self.selStandard == null || self.selSubject == null){
				alert("Please select Pattern, Standard and Subject.");
				return;
			}
		
			var code = self.selPattern.name.substr(0, 2) + '_' + self.selStandard.abbr + '_' + self.selSubject.name + '_' + self.newChap.no;
			var chapRef = firebase.database().ref('metadata/chapters/');
			var newChapRef = chapRef.push();
			
			newChapRef.set({
				name : self.newChap.name,
				no : self.newChap.no,
				seqNo : parseInt(self.newChap.seqNo),
				shortCode : code,
				thumb : code + '_01_Thumb.jpg',
				nav_path : self.selPattern.key + '_' + self.selStandard.key + '_' + self.selSubject.key
			});
			
			alert("New Chapter saved : " + self.newChap.name);
			self.newChap.key = newChapRef.key;
			self.chapters.push(self.newChap);
		}
		else {
			var code = self.selPattern.name.substr(0, 2) + '_' + self.selStandard.abbr + '_' + self.selSubject.name + '_' + self.newChap.no;
			firebase.database().ref('metadata/chapters/' + self.newChap.key)
				.set({
					name : self.newChap.name,
					no : self.newChap.no,
					seqNo : parseInt(self.newChap.seqNo),
					shortCode : code,
					thumb : code + '_01_Thumb.jpg',
					nav_path : self.selPattern.key + '_' + self.selStandard.key + '_' + self.selSubject.key
				});
			alert("Updated : " + self.newChap.name);
			self.loadChaptersList();
		}
		
		self.resetForm();
	};
	
	self.resetForm = function(){
		self.newChap = new ChapterModel();
		self.buttonText = "Create";
	};
};

angular.
  module('chapters').
  component('chaptersList', {
    templateUrl: 'chapters/chapters-list.template.html',
    controller: ListChaptersController
  });

 