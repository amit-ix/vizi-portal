'use strict';

angular.
  module('viziAdminApp').
  config(['$locationProvider' ,'$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/', {
          template: '<users-summary></users-summary>'
        }).
		when('/404', {
          templateUrl: '404.html'
        }).
		when('/patterns', {
          template: '<patterns-list></patterns-list>'
        }).
		when('/standards', {
          template: '<standards-list></standards-list>'
        }).
		when('/subjects', {
          template: '<subjects-list></subjects-list>'
        }).
		when('/chapters', {
          template: '<chapters-list></chapters-list>'
        }).
		when('/charts', {
          template: '<charts-list></charts-list>'
        }).
		when('/users', {
          template: '<users-summary></users-summary>'
        }).
		when('/usersList/:userType', {
          template: '<users-list></users-list>'
        }).
		when('/studentsList', {
          template: '<students-list></students-list>'
        }).
		when('/inappmsgs', {
          template: '<inappmsgs-list></inappmsgs-list>'
        }).
		when('/consumption/:date', {
          template: '<consumption-list></consumption-list>'
        }).
		when('/consMatrix', {
          template: '<consumption-matrix></consumption-matrix>'
        }).
		when('/studentConsumption/:key', {
          template: '<student-consumption></student-consumption>'
        }).
        otherwise('404');
    }
  ]);