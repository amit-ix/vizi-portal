'use strict';
function PerUserConsumption(){
	this.studentName = "";
	this.totalTime = "";
	this.perChapterTime = [];
}

function ListConsumptionController($scope, $http, $routeParams){
	var self = this;
	self.statusText = "Loading...";
	self.consumption = [];
	self.date = $routeParams.date;
	self.activeUserCount = 0;
	self.totalTimeSpent = 0;
	
	self.loadDailyConsumption = function(date) {
		firebase.database().ref('analytics-data/time-trackers/per-day/' + date).once('value', function(perDaySnapshot){
			self.consumption = [];
			self.activeUserCount = perDaySnapshot.val().total_active_users;
			self.totalTimeSpent = Utils.secondsToHms(perDaySnapshot.val().total_time);
			
			self.allStudentNames = [];
			var allChapterNames = [];
			
			firebase.database().ref('user-data/students').once('value', function(snapshot){
				snapshot.forEach(function (childSnapshot){
					self.allStudentNames[childSnapshot.key] = childSnapshot.val().name;
				})
			}).then(function(){
				firebase.database().ref('metadata/chapters').once('value', function(snapshot){
					snapshot.forEach(function (childSnapshot){
						allChapterNames[childSnapshot.key] = childSnapshot.val().shortCode + ": " + childSnapshot.val().name;
					})
				}).then(function(){
				perDaySnapshot.forEach(function (perStudentSnapshot){
					if(perStudentSnapshot.key.indexOf("total_") == -1){
						var userCons = new PerUserConsumption();
						userCons.studentName = self.allStudentNames[perStudentSnapshot.key];
						userCons.key = perStudentSnapshot.key;
						userCons.totalTime = Utils.secondsToHms(perStudentSnapshot.val().total_time);
						
						//for(var c in allChapterNames)
						
						perStudentSnapshot.forEach(function(perChapSnapshot){
							if(perChapSnapshot.key.indexOf("total_") == -1){
								var chapName = allChapterNames[perChapSnapshot.key];
								userCons.perChapterTime.push(chapName + ' : ' + Utils.secondsToHms(perChapSnapshot.val()));
							}
						})
						
						self.consumption.push(userCons);
					}					
				});
				
			}).then(function(){			
				$scope.$apply(function(){
					self.statusText = "";
				});	
			});
		});
	})
	};
	// Load list on page load
	self.loadDailyConsumption(self.date);
};

function ConsumptionMatrixController($scope, $http, $sce){
	var self = this;
	self.statusText = "Loading...";

	var allDates = [];
	var allStudents = {};
	var consumptionMatrix = {};
	
	self.loadConsumptionMatrix = function(){
		var ixUsers = [];
		
		firebase.database().ref('user-data/users').once('value', function(snapshot){
			snapshot.forEach(function(childSnapshot){
				if (childSnapshot.val().user_type == "IX"){
					ixUsers.push(childSnapshot.key);
				}
			})
		}).then(function(){
			
		firebase.database().ref('user-data/students').once('value', function(snapshot){
			snapshot.forEach(function(childSnapshot){
				if (ixUsers.indexOf(childSnapshot.val().userID) == -1){
					allStudents[childSnapshot.key] = childSnapshot.val().name;
				}
			})
			
			firebase.database().ref('analytics-data/time-trackers/per-day').once('value', function(daySnapshot){
				daySnapshot.forEach(function(perDay){
					allDates.push(perDay.key);
					var thisDayTimes = {};
									
					perDay.forEach(function(dayStud){
						if (dayStud.key.indexOf("total_") == -1 && allStudents[dayStud.key] != undefined){
							thisDayTimes[dayStud.key] = dayStud.val().total_time;						
						}					
					})
					consumptionMatrix[perDay.key] = thisDayTimes;
				})
				
				$scope.$apply(function(){
					self.prepareMatrixTable();
					self.studentsCount = Object.keys(allStudents).length;
					self.statusText = "";
				});
			});
		});	
		
		});
	};
	
	self.prepareMatrixTable = function(){
		self.matrixTable = "<table class='table table-striped table-bordered table-hover table-condensed'>";
		self.matrixTable += "<tr><th>Names</th>";
		
		//self.matrixTable += "<th>Key</th>";
		
		for (var dt in allDates){
			self.matrixTable += "<th>" + allDates[dt] + "</th>";
		}
		self.matrixTable += "</tr>";
		
		for (var nm in allStudents){
			self.matrixTable += "<tr>";
			self.matrixTable += "<td>" + allStudents[nm] + "</td>";
			
			//self.matrixTable += "<td>" + nm + "</td>";
			
			for (var dt in allDates){
				var cellText = consumptionMatrix[allDates[dt]][nm];
				
				cellText = cellText != undefined ? cellText : "";
				var title = allDates[dt] + " : " + allStudents[nm] + " = " + Utils.secondsToHms(cellText);
				self.matrixTable += "<td title='" + title +"'>" + cellText + "</td>";
			}
			
			self.matrixTable += "</tr>";
		}
		self.matrixTable += "</table>";
		
		document.getElementById("tblMatrix").innerHTML = self.matrixTable;
	};
	
	self.loadConsumptionMatrix();
};

angular.
  module('consumption').
  component('consumptionList', {
    templateUrl: 'consumption/consumption-list.template.html',
    controller: ListConsumptionController
  });
  
angular.
  module('consumption').
  component('consumptionMatrix', {
    templateUrl: 'consumption/consumption-matrix.template.html',
    controller: ConsumptionMatrixController
  });
 