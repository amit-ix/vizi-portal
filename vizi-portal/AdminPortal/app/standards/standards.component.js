'use strict';

function StandardModel(std_key, std_name, std_abbr, std_seqNo){
	this.key = std_key;
	this.name = std_name;
	this.abbr = std_abbr;
	this.seqNo = std_seqNo;
};

function ListStandardsController($scope, $http){
	var self = this;
	self.statusText = "Loading...";
	self.buttonText = "Create";
	self.standards = [];
	
	self.createUpdateStandard = function (){
		if (self.buttonText == "Create"){
			var stdRef = firebase.database().ref('metadata/standards');
			var newStdRef = stdRef.push();
			
			newStdRef.set({
				name : self.newStd.name,
				abbr : self.newStd.abbr,
				seqNo : self.newStd.seqNo
			});
			
			alert("New Standard saved : " + self.newStd.name);
			self.newStd.key = newStdRef.key;
			self.standards.push(self.newStd);
		}
		else {
			firebase.database().ref('metadata/standards/' + self.newStd.key)
				.set({
					name : self.newStd.name,
					abbr : self.newStd.abbr,
					seqNo : self.newStd.seqNo
				});
			alert("Updated : " + self.newStd.name);
			self.loadStandardsList();
		}
		
		self.resetForm();
	};
	
	self.resetForm = function(){
		self.newStd = new StandardModel();
		self.buttonText = "Create";
	};
	
	self.loadStandard = function(key){
		firebase.database().ref('metadata/standards/' + key)
			.once('value').then(function (snapshot){
				$scope.$apply(function(){
					self.newStd = new StandardModel(snapshot.key,
							snapshot.val().name,
							snapshot.val().abbr,
							snapshot.val().seqNo);
				});
			});
		self.buttonText = "Update";
	};
	
	self.loadStandardsList = function() {
		firebase.database().ref('metadata/standards').orderByChild('seqNo').once('value', function(snapshot){
			self.standards = [];
			snapshot.forEach(function (childSnapshot){
				var tmpStd = new StandardModel(childSnapshot.key,
												childSnapshot.val().name,
												childSnapshot.val().abbr,
												childSnapshot.val().seqNo);
				self.standards.push(tmpStd);
			});
		  
			$scope.$apply(function(){
				self.statusText = "";
			});
		});
	};
	
	// Load list on page load
	self.loadStandardsList();
};

angular.
  module('standards').
  component('standardsList', {
    templateUrl: 'standards/standards-list.template.html',
    controller: ListStandardsController
  });
  
 