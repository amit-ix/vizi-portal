'use strict';

function SubjectModel(sub_key, sub_name, sub_color, sub_thumb){
	this.key = sub_key;
	this.name = sub_name;
	this.color = sub_color;
	this.thumb = sub_thumb;
};

function ListSubjectsController($scope, $http, firebaseStorageSvc){
	var self = this;
	self.statusText = "Loading...";
	self.buttonText = "Create";
	self.thumbPreviewUrl = "";
	
	self.newSub  = new SubjectModel();
	
	self.createUpdateSubject = function (imgStoragePath){

		if (self.buttonText == "Create"){
			var subjectRef = firebase.database().ref('metadata/subjects');
			var newSubjectRef = subjectRef.push();
			
			newSubjectRef.set({
				name : self.newSub.name,
				color : self.newSub.color,
				thumb : imgStoragePath
			});
			alert("New subject saved : " + self.newSub.name);
			self.newSub.key = newSubjectRef;
			self.subjects.push(self.newSub);
		}
		else {
			firebase.database().ref('metadata/subjects/' + self.newSub.key)
				.set({
					key : self.newSub.key,
					name : self.newSub.name,
					color : self.newSub.color,
					thumb : imgStoragePath
				});
			alert("Updated : " + self.newSub.name);
			self.loadSubjectsList();
		};
		
		self.resetForm();
	}
	
	self.resetForm = function(){
		self.newSub = new SubjectModel();
		self.buttonText = "Create";
		self.fileStatus = "";
		self.imgFile = null;
		self.thumbPreviewUrl = "";
	};
	
	self.uploadThumbnail = function(){
		if ($scope.imgFile == null || $scope.imgFile == undefined){
			alert("Please select Thumbnail image.");
			return;
		}
		
		self.fileStatus = "Uploading file...";
		// Upload file to firebase
		var filePath = "Subject Icon";
		
		var uploadStatus = firebaseStorageSvc.uploadFile($scope.imgFile, filePath, self.createUpdateSubject);
	};
	
	self.loadSubject = function(key){
		var subjectRef = firebase.database().ref('metadata/subjects/' + key)
							.once('value').then(function (snapshot){
								$scope.$apply(function(){
									self.newSub = new SubjectModel(snapshot.key,
										snapshot.val().name,
										snapshot.val().color,
										snapshot.val().thumb);
									
									self.thumbPreviewUrl = "";
								});
								
								firebaseStorageSvc.setImagePath(snapshot.val().thumb).then(function(url){
									$scope.$apply(function(){
										self.thumbPreviewUrl = url;
									});	
								});
							});
		self.buttonText = "Update";
	};
		
	self.loadSubjectsList = function(){
		firebase.database().ref('metadata/subjects').once('value', function(snapshot){
			self.subjects = [];
			snapshot.forEach(function (childSnapshot){
				var tmpSub = new SubjectModel(childSnapshot.key,
												childSnapshot.val().name,
												childSnapshot.val().color,
												childSnapshot.val().thumb);
				self.subjects.push(tmpSub);
			});
		  
			$scope.$apply(function(){
				self.statusText = "";
			});
		});
	};
	
	// Load list on page load
	self.loadSubjectsList();
};

angular.
  module('subjects').
  component('subjectsList', {
    templateUrl: 'subjects/subjects-list.template.html',
    controller: ListSubjectsController
  });

 