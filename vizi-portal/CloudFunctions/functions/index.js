/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for t`he specific language governing permissions and
 * limitations under the License.
 */
'use strict';

const functions = require('firebase-functions');
const mkdirp = require('mkdirp-promise');
// Include a Service Account Key to use a Signed URL
const gcs = require('@google-cloud/storage')({
	//projectId: 'vizcharts-dev',
	projectId: 'vizi-prod',
	keyFilename: 'prod-service-account-credentials.json'});
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);
const spawn = require('child-process-promise').spawn;
const path = require('path');
const os = require('os');
const fs = require('fs');

// Max height and width of the thumbnail in pixels.
const THUMB_MAX_HEIGHT = 500;
const THUMB_MAX_WIDTH = 500;
// Thumbnail prefix added to file names.
const THUMB_PREFIX = 'thumb_';

/**
 * When an image is uploaded in the Storage bucket We generate a thumbnail automatically using
 * ImageMagick.
 * After the thumbnail has been generated and uploaded to Cloud Storage,
 * we write the public URL to the Firebase Realtime Database.
 */
exports.generateThumbnail = functions.storage.object().onChange(event => {
  // File and directory paths.
  const filePath = event.data.name;
  const fileDir = path.dirname(filePath);
  const fileName = path.basename(filePath);
  const thumbFilePath = path.normalize(path.join(fileDir, `${THUMB_PREFIX}${fileName}`));
  const tempLocalFile = path.join(os.tmpdir(), filePath);
  const tempLocalDir = path.dirname(tempLocalFile);
  const tempLocalThumbFile = path.join(os.tmpdir(), thumbFilePath);

  // Exit if this is triggered on a file that is not an image.
  if (!event.data.contentType.startsWith('image/')) {
    console.log('This is not an image.');
    return;
  }

  // Exit if the image is already a thumbnail.
  if (fileName.startsWith(THUMB_PREFIX)) {
    console.log('Already a Thumbnail.');
    return;
  }

  // Exit if this is a move or deletion event.
  if (event.data.resourceState === 'not_exists') {
    console.log('This is a deletion event.');
    return;
  }

  // Cloud Storage files.
  const bucket = gcs.bucket(event.data.bucket);
  const file = bucket.file(filePath);
  const thumbFile = bucket.file(thumbFilePath);

  // Create the temp directory where the storage file will be downloaded.
  return mkdirp(tempLocalDir).then(() => {
    // Download file from bucket.
    return file.download({destination: tempLocalFile});
  }).then(() => {
    console.log('The file has been downloaded to', tempLocalFile);
    // Generate a thumbnail using ImageMagick.
    return spawn('convert', [tempLocalFile, '-thumbnail', `${THUMB_MAX_WIDTH}x${THUMB_MAX_HEIGHT}>`, tempLocalThumbFile]);
  }).then(() => {
    console.log('Thumbnail created at', tempLocalThumbFile);
    // Uploading the Thumbnail.
    return bucket.upload(tempLocalThumbFile, {destination: thumbFilePath});
  }).then(() => {
    console.log('Thumbnail uploaded to Storage at', thumbFilePath);
    // Once the image has been uploaded delete the local files to free up disk space.
    fs.unlinkSync(tempLocalFile);
    fs.unlinkSync(tempLocalThumbFile);  
  });
});

exports.captureUserCount = functions.database.ref('user-data/users/{key}').onWrite(event =>{
	var totalUsers = 0;
	var ixUsers = 0;
	var parentUsers = 0;
	var totalStudents = 0;
	
	var ixUserChange = 0;
	var parentUserChange = 0;
	var totalCountChange = 0;
	
	var changed = false;
	
	// When user is first created
	if(!event.data.previous.exists()){
		console.log('New user created : ' + event.data.child('name').val());
		totalCountChange = 1;
		if (event.data.child('user_type').val() == "IX"){
			ixUserChange = 1;
		} 
		else {
			parentUserChange = 1;
		}
		changed = true;
	}
	
	// When user is deleted
	else if(!event.data.exists()){
		console.log('Deleted user : ' + event.data.previous.child('name').val());
		totalCountChange = - 1;
		if (event.data.previous.child('user_type').val() == "IX"){
			ixUserChange = - 1;
		}
		else if (event.data.previous.child('user_type').val() != "IX"){
			parentUserChange = - 1;
		}
		changed = true;
	}
	else {
		// When data is updated, check if the user_type is changed on not
		var userTypeRef = event.data.child('user_type');
		if (userTypeRef.changed()){
			console.log("User Type changed to : " + userTypeRef.val());
			if (userTypeRef.val() == "IX"){
				ixUserChange = 1;
				parentUserChange = - 1;
			}
			else if (userTypeRef.val() != "IX"){
				ixUserChange = - 1;
				parentUserChange = 1;
			}
			changed = true;
		}
	}
	
	if(!changed)
		return;
	
	admin.database().ref('analytics-data/user-counts').once('value', function(snapshot){
		if (snapshot != null || snapshot != undefined){
			totalUsers = snapshot.val().total_users;
			ixUsers = snapshot.val().ix_users;
			parentUsers = snapshot.val().parents;
			totalStudents = snapshot.val().total_students;
		}

		totalUsers += totalCountChange;
		parentUsers += parentUserChange;
		ixUsers += ixUserChange;
		
		console.log('Updating user counters : ' + totalUsers + ' - ' + ixUsers + ' - ' + parentUsers + ' - ' + totalStudents);
		admin.database().ref('analytics-data/user-counts').set({
			total_users : totalUsers,
			ix_users : ixUsers,
			parents : parentUsers,
			total_students : totalStudents
		});
	});
});

exports.captureStudentCount = functions.database.ref('user-data/students/{student-key}').onWrite(event =>{
	var totalStudents = 0;
	
	var newStd = "";
	var oldStd = "";
	
	admin.database().ref('analytics-data/user-counts/total_students').once('value', function(snapshot){
		totalStudents = snapshot.val();
	
		// When student is first created
		if(!event.data.previous.exists()){	
			totalStudents = totalStudents + 1;
			newStd = event.data.child('standard/key').val();
			oldStd = "";
			console.log('Student created. New Count : ' + totalStudents);
		}
		
		// When student is deleted
		else if(!event.data.exists()){	
			totalStudents = totalStudents - 1;
			oldStd = event.data.previous.child('standard/key').val();
			newStd = "";
			console.log('Student deleted. New Count : ' + totalStudents);
		}
		else {
			newStd = event.data.child('standard/key').val();
			oldStd = event.data.previous.child('standard/key').val();
			if (newStd == oldStd){
				console.log('Student standard not changed');
				return;
			}
			console.log('Student standard changed from: ' + oldStd + ',  to: ' + newStd);
		}
		
		admin.database().ref('analytics-data/user-counts/total_students').set(totalStudents);
		
		if (newStd != ""){
			admin.database().ref('analytics-data/students-per-std/' + newStd).once('value', function(snapshot){
				var studCount = snapshot.exists() ? snapshot.val() + 1 : 1;
				admin.database().ref('analytics-data/students-per-std/' + newStd).set(studCount);
			});
		}
		if (oldStd != ""){
			admin.database().ref('analytics-data/students-per-std/' + oldStd).once('value', function(snapshot){
				var studCount = snapshot.exists() ? snapshot.val() - 1 : 0;
				admin.database().ref('analytics-data/students-per-std/' + oldStd).set(studCount);
			});
		}
	});
	
});

exports.captureChartViewTime = functions.database.ref('audit-data/chart-view-event/{key}').onCreate(event => {
  	if (event.data.child('event_name').val() != "Chapter Image View"){
		return;
	}
	
	var time = parseInt(event.data.child('view_duration').val());
	if(isNaN(time))
		return;
  
	console.log("Chart View : " + event.data.child('chapter_image').val() + ", time : " + time);
	var imgLocation = event.data.child('chapter_image').val();
	var parts = imgLocation.split("/");
	
	// Increment total time
	admin.database().ref('analytics-data/time-trackers/total_time_spent').once('value', function(snapshot){
		var total = snapshot.exists() ? snapshot.val() : 0;
		total = total + time;
		admin.database().ref('analytics-data/time-trackers/total_time_spent').set(total);
	});
		
	// Increment standard count
	var stdKey = parts[0];
	admin.database().ref('analytics-data/time-trackers/per-standard/' + stdKey).once('value', function(snapshot){
		var total = snapshot.exists() ? snapshot.val() : 0;
		total = total + time;
		admin.database().ref('analytics-data/time-trackers/per-standard/' + stdKey).set(total);
	});
	
	// Increment Subject count
	var subKey = parts[0] + "~" + parts[1];
	admin.database().ref('analytics-data/time-trackers/per-subject/' + subKey).once('value', function(snapshot){
		var total = snapshot.exists() ? snapshot.val() : 0;
		total = total + time;
		admin.database().ref('analytics-data/time-trackers/per-subject/' + subKey).set(total);
	});
	
	// Increment Chapter count
	var chapKey = event.data.child('chapter_name').val();
	admin.database().ref('analytics-data/time-trackers/per-chapter/' + chapKey).once('value', function(snapshot){
		var total = snapshot.exists() ? snapshot.val() : 0;
		total = total + time;
		admin.database().ref('analytics-data/time-trackers/per-chapter/' + chapKey).set(total);
	});
	
	// Increment Student total time and per chapter count
	var studKey = event.data.child('student_id').val();
	admin.database().ref('analytics-data/time-trackers/per-student/' + studKey + '/total_time').once('value', function(snapshot){
		var total = snapshot.exists() ? snapshot.val() : 0;
		total = total + time;
		admin.database().ref('analytics-data/time-trackers/per-student/' + studKey + '/total_time').set(total);
	});
	admin.database().ref('analytics-data/time-trackers/per-student/' + studKey + '/' + chapKey).once('value', function(snapshot){
		var total = snapshot.exists() ? snapshot.val() : 0;
		total = total + time;
		admin.database().ref('analytics-data/time-trackers/per-student/' + studKey + '/' + chapKey).set(total);
	});
	
	// Increment per day timer count
	var dt = event.data.child('timestamp').val() != undefined ? new Date(event.data.child('timestamp').val()) : new Date(event.data.child('ist_time_stamp').val());
						
	if (isNaN(dt.getMonth())){
		var tmp = childSnapshot.val().timestamp.replace("IST", "GMT+05:30");
		dt = new Date(tmp);
	}
	if (!isNaN(dt.getMonth())){							
		var date = (dt.getMonth() + 1) < 10 ? "0" + (dt.getMonth() + 1) : (dt.getMonth() + 1);
		date = date + '-' + (dt.getDate() < 10 ? "0" + dt.getDate() : dt.getDate());
		date = date + '-' + dt.getFullYear();	
		
		admin.database().ref('analytics-data/time-trackers/per-day/' + date).once('value', function(snapshot){
			if (snapshot.exists()){
				admin.database().ref('analytics-data/time-trackers/per-day/' + date + '/total_time').set(snapshot.val().total_time + time);
			}
			else{
				admin.database().ref('analytics-data/time-trackers/per-day/' + date + '/total_time').set(time);
				admin.database().ref('analytics-data/time-trackers/per-day/' + date + '/total_active_users').set(0);
			}
		});
		admin.database().ref('analytics-data/time-trackers/per-day/' + date + '/' + studKey + '/' + chapKey).once('value', function(snapshot){
			var total = snapshot.exists() ? snapshot.val() : 0;
			total = total + time;
			
			admin.database().ref('analytics-data/time-trackers/per-day/' + date + '/' + studKey + '/' + chapKey).set(total).then(function(){
				// increment active user count
				admin.database().ref('analytics-data/time-trackers/per-day/' + date).once('value', function(snapshot){
					admin.database().ref('analytics-data/time-trackers/per-day/' + date + '/total_active_users').set(snapshot.numChildren() - 2);
				});
			});
		});
		
		admin.database().ref('analytics-data/time-trackers/per-day/' + date + '/' + studKey + '/total_time').once('value', function(snapshot){
			var total = snapshot.exists() ? snapshot.val() + time : time;
			admin.database().ref('analytics-data/time-trackers/per-day/' + date + '/' + studKey + '/total_time').set(total);
		});
	}
	else{
		firebase.database().ref('analytics-data/time-trackers/per-day-unprocessed/' + childSnapshot.key).set(childSnapshot.val());
	}
});

exports.moveLegacyChapterViewEvent = functions.database.ref('audit-data/user-event/{key}').onCreate(event => {
	if (event.data.child('event_name').val() != "Chapter Image View"){
		return;
	}
	
	console.log("Moving event to new location : " + event.data.child('chapter_image').val());
	
	admin.database().ref('audit-data/chart-view-event/' + event.params.key).set(event.data.val());
	admin.database().ref('audit-data/chart-view-event-backup/' + event.params.key).set(event.data.val());
	admin.database().ref('audit-data/user-event/' + event.params.key).remove();
});

exports.captureAppVersionCount = functions.database.ref('audit-data/user-device-info/{userKey}/app_version_name').onWrite(event => {
	var newVersion = "";
	var oldVersion = "";
	
	if(!event.data.previous.exists()){	
		newVersion = event.data.val();
	}
	else if(!event.data.exists()){	
		oldVersion = event.data.previous.val();
	}
	else {
		newVersion = event.data.val();
		oldVersion = event.data.previous.val();
	}
	
	// Increase new version count
	if (newVersion != undefined && newVersion != ""){
		newVersion = newVersion.replace(/[.]/g, "-");
		admin.database().ref('analytics-data/app-version-counts/' + newVersion).once('value', function(snapshot){
			var count = snapshot.exists() ? snapshot.val() + 1 : 1;
			admin.database().ref('analytics-data/app-version-counts/' + newVersion).set(count);
		});
	}
	// Decrement old version count
	if (oldVersion != undefined && oldVersion != ""){
		oldVersion = oldVersion.replace(/[.]/g, "-");
		admin.database().ref('analytics-data/app-version-counts/' + oldVersion).once('value', function(snapshot){
			var count = snapshot.exists() ? snapshot.val() - 1 : 0;
			admin.database().ref('analytics-data/app-version-counts/' + oldVersion).set(count);
		});
	}
});


